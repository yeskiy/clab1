#include "tasks.h"
#include "utils.h"
#include <cmath>
#include <algorithm>

void firstTask() {
    const int firstCircleRadius = requireInt("Type the first circle radius: ");
    const int secondCircleRadius = requireInt("Type the second circle radius: ");
    if (firstCircleRadius < secondCircleRadius) {
        throw RuntimeException("The radius of the first circle must be greater than the second");
    }
    const double firstCircleArea = customPI * pow(firstCircleRadius, 2.0);
    const double secondCircleArea = customPI * pow(secondCircleRadius, 2.0);
    const double ringArea = customPI * (pow(firstCircleRadius, 2.0) - pow(secondCircleRadius, 2.0));
    print("First Circle Area:", firstCircleArea);
    print("Second Circle Area:", secondCircleArea);
    print("Ring Area:", ringArea);
}

void secondTask() {
    const int firstX = requireInt("Type the x1: ");
    const int firstY = requireInt("Type the y1: ");
    const int secondX = requireInt("Type the x2: ");
    const int secondY = requireInt("Type the y2: ");
    if (firstX == secondX) {
        throw RuntimeException("x1 cannot be equal to x2");
    }
    if (firstY == secondY) {
        throw RuntimeException("y1 cannot be equal to y2");
    }
    const double a = abs(firstX - secondX);
    const double b = abs(firstY - secondY);
    const double size = 2 * (a + b);
    const double area = a * b;
    print("Perimeter of Rectangle:", size);
    print("Rectangle Area:", area);
}

void thirdTask() {
    const int firstX = requireInt("Type the x1: ");
    const int firstY = requireInt("Type the y1: ");
    const int secondX = requireInt("Type the x2: ");
    const int secondY = requireInt("Type the y2: ");
    const double distance = sqrt(pow(secondX - firstX, 2.0) + pow(secondY - firstY, 2.0));
    print("Distance Between two pointers is:", distance);
}

void fourthTask() {
    const int firstX = requireInt("Type the x1: ");
    const int firstY = requireInt("Type the y1: ");
    const int secondX = requireInt("Type the x2: ");
    const int secondY = requireInt("Type the y2: ");
    const int thirdX = requireInt("Type the x3: ");
    const int thirdY = requireInt("Type the y3: ");
    const double size = sqrt(pow(secondX - firstX, 2.0) + pow(secondY - firstY, 2.0)) +
                        sqrt(pow(thirdX - secondX, 2.0) + pow(thirdY - secondY, 2.0)) +
                        sqrt(pow(firstX - thirdX, 2.0) + pow(firstY - thirdY, 2.0));
    print("Triangle Perimeter:", size);
}

void fifthTask() {
    const int a = requireInt("Type the a: ");
    const int b = requireInt("Type the b: ");
    const double size = 2 * (a + b);
    const double area = a * b;
    print("Perimeter of Rectangle:", size);
    print("Rectangle Area:", area);
}

void sixthTask() {
    const int a = requireInt("Type the A: ");
    const int b = requireInt("Type the B: ");
    const int c = requireInt("Type the C: ");
    const double volume = a * b * c;
    const double area = 2 * (a * b + a * c + b * c);
    print("Volume of rectangular parallelepiped:", volume);
    print("area of rectangular parallelepiped:", area);
}

void seventhTask() {
    const int a = requireInt("Type the A: ");
    print("a^2:", pow(a, 2.00));
    print("a^3:", pow(a, 3.00));
    print("a^5:", pow(a, 5.00));
    print("a^10:", pow(a, 10.00));
    print("a^15:", pow(a, 15.00));
}

void eighthTask() {
    const int a = requireInt("Type Fahrenheit temperature: ");
    print("Celsius temperature:", (a - 32) * 5 / 9);
}

void ninthTask() {
    const int a = requireInt("Type Celsius temperature: ");
    print("Fahrenheit temperature:", a * 9 / 5 + 32);
}

void tenthTask() {
    const int x = requireInt("Type the X: ");
    const int a = requireInt("Type the A: ");
    const int y = requireInt("Type the Y: ");
    const int b = requireInt("Type the B: ");
    const float ax = (float) a / (float) x;
    const float by = (float) b / (float) y;
    if (ax < by) {
        throw RuntimeException("1 Kilo of Chocolate Sweets must be more expensive than 1 Kilo of Toffee");
    }
    print("1 Kilo of Chocolate Sweets costs", ax, "g.");
    print("1 Kilo of Toffee costs", by, "g.");
    print("1 Kilo of Chocolate Sweets more expensive than 1 Kilo of Toffee on", ax / by, "x");
}

void eleventhTask() {
    const double a = pow(requireInt("Type the A: "), 2.00);
    const double b = pow(requireInt("Type the B: "), 2.00);
    print("Powered sum:", a + b);
    print("Powered subtract:", a - b);
    print("Powered multiplication:", a * b);
    print("Powered division:", a / b);
}

void twelfthTask() {
    const double a = abs(requireInt("Type the A: "));
    const double b = abs(requireInt("Type the B: "));
    print("Abs sum:", a + b);
    print("Abs subtract:", a - b);
    print("Abs multiplication:", a * b);
    print("Abs division:", a / b);
}

void thirteenthTask() {
    const double a = abs(requireInt("Type the A: "));
    const double b = abs(requireInt("Type the B: "));
    const double c = sqrt(pow(a, 2.0) + pow(b, 2.0));
    print("C is:", c);
    print("Perimeter of Triangle:", a + b + c);
}

void fourteenthTask() {
    const int v1 = abs(requireInt("Type the V1: "));
    const int v2 = abs(requireInt("Type the V2: "));
    const int s = abs(requireInt("Type the S: "));
    const int t = abs(requireInt("Type the T: "));
    print("distance between cars is:", (v2 * t + s) - v1 * t);
};

void fifteenthTask() {
    const int v1 = abs(requireInt("Type the V1: "));
    const int v2 = abs(requireInt("Type the V2: "));
    const int s = abs(requireInt("Type the S: "));
    const int t = abs(requireInt("Type the T: "));
    print("distance between cars is:", s - (v1 * t + v2 * t));
};